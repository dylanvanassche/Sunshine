project('sunshine',
          version: '0.2.0',
    meson_version: '>= 0.50.0',
  default_options: [ 'warning_level=2',
                   ],
)

dependency('libhandy-1', version: '>= 1.0.0')

i18n = import('i18n')

envdata = environment()
python_paths = [join_paths(meson.current_build_dir(), '..')]
envdata.append('PYTHONPATH', python_paths)
envdata.append('TESTS_BUILDDIR', meson.current_build_dir())
configure_file(input : 'mypy.ini',
               output : 'mypy.ini',
               copy: true)

test(
  'Unittests',
  import('python').find_installation('python3'),
  args: ['-m', 'unittest', 'tests'],
  env: envdata
)

test(
  'MyPy static type check',
  import('python').find_installation('python3'),
  args: ['-m', 'mypy', join_paths(meson.current_source_dir(), 'src')],
  env: envdata
)

test(
  'PEP8 compliance check',
  import('python').find_installation('python3'),
  args: ['-m', 'flake8', join_paths(meson.current_source_dir(), 'src')],
  env: envdata
)

subdir('data')
subdir('src')
subdir('tests')
subdir('po')

meson.add_install_script('build-aux/meson/postinstall.py')
