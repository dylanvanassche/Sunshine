# local.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import List, cast, TYPE_CHECKING
from os import makedirs
from xdg import BaseDirectory
from pathlib import Path
from logging import debug
from rdflib import URIRef, Graph
from requests import get
from urllib.parse import urlparse

from .storage import Storage
from ..sunshine import STORAGE_DIR, STORAGE_DIR_SHOW, STORAGE_DIR_COVER, \
                       STORAGE_DIR_SEASON, STORAGE_DIR_EPISODE, BASE_URL, \
                       RDF_FORMAT, SCHEMA

if TYPE_CHECKING:
    from requests import Response


class LocalStorage(Storage):
    __instance = None

    @staticmethod
    def get_instance():
        """
        Singleton design pattern
        """
        if LocalStorage.__instance is None:
            LocalStorage()

        return LocalStorage.__instance

    def __init__(self):
        # Singleton design pattern
        if LocalStorage.__instance is not None:
            raise Exception('This class is a singleton!')
        else:
            LocalStorage.__instance = self

        super().__init__()
        self._dir_path: Path = Path(BaseDirectory.save_data_path(STORAGE_DIR))
        self._dir_path = self._dir_path.absolute()

    def _show_path(self, iri: URIRef) -> Path:
        path: Path
        show_id: str
        iri_path: str

        # Determine local storage path for show
        # /show/$show_id
        iri_path = urlparse(iri.toPython()).path
        show_id = iri_path.split('/')[2]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_SHOW}/{show_id}') \
                             .absolute()
        return path

    def _season_path(self, iri: URIRef) -> Path:
        path: Path
        season_id: str
        iri_path: str

        # Determine local storage path for season
        # /show/$show_id/season/$season_id
        iri_path = urlparse(iri.toPython()).path
        season_id = iri_path.split('/')[4]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_SEASON}/{season_id}') \
                             .absolute()
        return path

    def _episode_path(self, iri: URIRef) -> Path:
        path: Path
        episode_id: str
        iri_path: str

        # Determine local storage path for episode
        # /show/$show_id/season/$season_id/episode/$episode_id
        iri_path = urlparse(iri.toPython()).path
        episode_id = iri_path.split('/')[6]
        path = self._dir_path.joinpath(f'{STORAGE_DIR_EPISODE}/{episode_id}') \
                             .absolute()
        return path

    def store_show(self, iri: URIRef, g: Graph) -> None:
        path: Path

        debug(f'Storing show {iri}')
        path = self._show_path(iri)
        makedirs(str(path.parent), exist_ok=True)
        g.serialize(path, format=RDF_FORMAT)

    def store_season(self, iri: URIRef, g: Graph) -> None:
        path: Path

        debug(f'Storing season {iri}')
        path = self._season_path(iri)
        makedirs(str(path.parent), exist_ok=True)
        g.serialize(path, format=RDF_FORMAT)

    def store_episode(self, iri: URIRef, g: Graph,
                      refresh: bool = False) -> None:
        path: Path

        debug(f'Storing episode {iri}, with refresh? {refresh}')
        path = self._episode_path(iri)
        makedirs(str(path.parent), exist_ok=True)

        # Save watch status before refreshing episode
        if refresh:
            g_show: Graph = Graph()
            g_show.parse(path, format=RDF_FORMAT)
            watch_action_iri: URIRef = g_show.value(iri,
                                                    SCHEMA.potentialAction,
                                                    any=False)
            watch_status: URIRef = g_show.value(watch_action_iri,
                                                SCHEMA.actionStatus,
                                                any=False)
            g.set((watch_action_iri, SCHEMA.actionStatus, watch_status))

        g.serialize(path, format=RDF_FORMAT)

    def retrieve_shows(self) -> List[URIRef]:
        shows: List[URIRef] = []

        for path in self._dir_path.glob(f'{STORAGE_DIR_SHOW}/*'):
            iri: URIRef = URIRef(str(path.absolute())
                                 .replace(str(self._dir_path), BASE_URL))

            debug(f'Loading show {iri} from file: {str(path)}')
            shows.append(iri)

        return shows

    def retrieve_show(self, iri: URIRef) -> Graph:
        """
        Retrieve a show as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._show_path(iri)
        debug(f'Loading show from file: {str(path)}')

        return cast(Graph, g.parse(str(path), format=RDF_FORMAT))

    def retrieve_season(self, iri: URIRef) -> Graph:
        """
        Retrieve a season as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._season_path(iri)
        debug(f'Loading season from file: {str(path)}')

        return cast(Graph, g.parse(str(path), format=RDF_FORMAT))

    def retrieve_episode(self, iri: URIRef) -> Graph:
        """
        Retrieve a episode as RDF from the storage.
        """
        path: Path
        g: Graph = Graph()

        path = self._episode_path(iri)
        debug(f'Loading episode from file: {str(path)}')

        return cast(Graph, g.parse(str(path), format=RDF_FORMAT))

    def retrieve_cover(self, iri: URIRef, url: str) -> Path:
        """
        Retrieve a cover image by URL and save it into storage.
        """
        p: str = str(iri).replace(f'{BASE_URL}/show',
                                  f'{STORAGE_DIR_COVER}')
        path: Path = self._dir_path.joinpath(p).absolute().resolve()
        makedirs(str(path.parent), exist_ok=True)

        # Cover may exist already in storage, return path in that case
        if path.exists():
            return path

        # Not downloaded yet, get cover and store it locally
        response: Response = get(url, stream=True)
        with open(path, 'wb') as f:
            for chunk in response:
                f.write(chunk)

        return path

    def remove_show(self, iri: URIRef) -> None:
        """
        Remove show by IRI from storage.
        """
        show_graph: Graph = self.retrieve_show(iri)

        # Remove all seasons
        for season_iri in show_graph.objects(iri, SCHEMA.containsSeason):
            season_iri = cast(URIRef, season_iri)
            season_graph: Graph = self.retrieve_season(season_iri)

            # Remove all episodes
            for episode_iri in season_graph.objects(season_iri,
                                                    SCHEMA.episode):
                episode_iri = cast(URIRef, episode_iri)

                # Remove episode from storage
                self._episode_path(episode_iri).unlink()

            # Remove season from storage
            self._season_path(season_iri).unlink()

        # Remove show from storage
        self._show_path(iri).unlink()
        debug(f'Show {iri} removed')

    @property
    def has_shows(self) -> bool:
        # If we have at least one show, return True
        try:
            next(self._dir_path.glob(f'{STORAGE_DIR_SHOW}/*'))
            return True
        except StopIteration:
            return False
