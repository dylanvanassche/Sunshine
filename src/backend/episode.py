# episode.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from typing import Optional, TYPE_CHECKING
from logging import debug
from datetime import date as d

from ..sunshine import SCHEMA
from .local import LocalStorage

if TYPE_CHECKING:
    from rdflib import URIRef, Graph
    from .storage import Storage


class Episode:
    """
    Data model for storing an episode of a show.
    """

    def __init__(self, iri: URIRef, graph: Optional[Graph] = None):
        self._iri: URIRef = iri
        self._storage: Storage = LocalStorage.get_instance()
        self._graph: Graph

        if graph is None:
            self._graph = self._storage.retrieve_episode(iri)
        else:
            self._graph = graph

    @property
    def iri(self) -> URIRef:
        """
        IRI of the episode.
        """
        return self._iri

    @property
    def identifier(self) -> str:
        """
        Episode identifier
        """
        if not hasattr(self, '_identifier'):
            self._identifier: str = self._graph.value(self._iri,
                                                      SCHEMA.identifier,
                                                      any=False).toPython()
        return self._identifier

    @property
    def title(self) -> str:
        """
        Episode title
        """
        if not hasattr(self, '_title'):
            self._title: str = self._graph.value(self._iri,
                                                 SCHEMA.name,
                                                 any=False).toPython()
        return self._title

    @property
    def number(self) -> int:
        """
        Episode number
        """
        if not hasattr(self, '_number'):
            self._number: int = self._graph.value(self._iri,
                                                  SCHEMA.episodeNumber,
                                                  any=False).toPython()
        return self._number

    @property
    def date(self) -> Optional[d]:
        """
        Episode date
        """
        if not hasattr(self, '_date'):
            self._date: Optional[d] = None
            try:
                self._date = self._graph.value(self._iri,
                                               SCHEMA.datePublished,
                                               any=False).toPython()
                if not isinstance(self._date, d):
                    self._date = None
                    debug(f'Invalid date for episode ({self.iri}): '
                          f'"{self._date}"')
            except AttributeError:
                pass
        return self._date

    @property
    def description(self) -> Optional[str]:
        """
        Episode description
        """
        if not hasattr(self, '_description'):
            self._description: Optional[str] = None
            try:
                self._description = self._graph.value(self._iri,
                                                      SCHEMA.description,
                                                      any=False).toPython()
            except AttributeError:
                pass
        return self._description

    @property
    def watched(self) -> bool:
        """
        If the episode was watched or not
        """
        if not hasattr(self, '_watched'):
            self._watched: bool = False
            watch_action_iri: URIRef = \
                self._graph.value(self._iri, SCHEMA.potentialAction, any=False)
            watch_action_status: URIRef = \
                self._graph.value(watch_action_iri, SCHEMA.actionStatus,
                                  any=False)

            if watch_action_status == SCHEMA.CompletedActionStatus:
                self._watched = True
            else:
                self._watched = False

        return self._watched

    @watched.setter
    def watched(self, watched: bool) -> None:
        """
        Update the episode watch state
        """
        if self.date is not None and self.date > d.today():
            debug('Episode watch state not changed, '
                  'episode is not released yet')
            return

        self._watched = watched

        watch_action_iri: URIRef = self._graph.value(self._iri,
                                                     SCHEMA.potentialAction,
                                                     any=False)

        if self._watched:
            self._graph.set((watch_action_iri,
                             SCHEMA.actionStatus,
                             SCHEMA.CompletedActionStatus))
        else:
            self._graph.set((watch_action_iri,
                             SCHEMA.actionStatus,
                             SCHEMA.PotentialActionStatus))

        self._storage.store_episode(self._iri, self._graph)
