# datasource.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from rdflib import URIRef
    from .show import Show
    from .search_result import SearchResult


class Datasource(ABC):
    """
    Abstraction of a datasource of TV shows.
    All datasources must implement this abstract class to be able to switch
    datasources without changing anything else of the program.
    """

    @abstractmethod
    def search(self, query) -> List[SearchResult]:
        """
        Search for TV shows with a given query
        """
        pass

    @abstractmethod
    def show(self, iri: URIRef, refresh: bool) -> Show:
        """
        Retrieve all information about a TV show by ID
        """
        pass
