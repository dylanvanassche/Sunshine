# kg.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

import re
import sys
from typing import List, Dict, Iterable, cast, TYPE_CHECKING
from logging import info, debug
from pathlib import Path
from requests import Session
from requests_cache import CachedSession
from rdflib import Graph, URIRef, RDF
from xdg import BaseDirectory
from tempfile import NamedTemporaryFile

from .local import LocalStorage
from .datasource import Datasource
from .show import Show
from .search_result import SearchResult
from ..sunshine import SCHEMA, TREE, STORAGE_DIR, RDF_FORMAT

if TYPE_CHECKING:
    from .storage import Storage
    from requests import Response
    from tempfile import _TemporaryFileWrapper
    from rdflib.term import Node

SEARCH_INDEX_IRI = 'https://sunshine.dylanvanassche.be/search/'


class KG(Datasource):
    """
    Datasource implementation of the Sunshine Knowledge Graph
    for retrieving TV shows information.
    """

    def __init__(self):
        super().__init__()
        self._storage: Storage = LocalStorage.get_instance()
        cache_path: Path = Path(BaseDirectory.save_cache_path(STORAGE_DIR))
        self._session: Session = CachedSession(str(cache_path))
        info(f'Cache installed at {cache_path}')

    def _clean_string(self, data: str) -> str:
        return data.lower().replace(':', '').replace('-', '').replace('\'', '')

    def _search(self, query: str, results: List[Dict] = [],
                iri: URIRef = URIRef(SEARCH_INDEX_IRI), path: URIRef = None):
        current_node: str = \
            self._clean_string(str(iri).replace(SEARCH_INDEX_IRI, ''))
        g: Graph = Graph()
        temp_file: _TemporaryFileWrapper = NamedTemporaryFile()
        response: Response

        # Download and read graph from Sunshine KG
        # RDFLib can do this behind the scenes but without support for caching
        response = self._session.get(str(iri), stream=True)
        debug(f'Fragment: {iri}')

        with open(temp_file.name, 'wb') as f:
            for chunk in response:
                f.write(chunk)
        g.parse(temp_file, format=RDF_FORMAT)

        # Find matches in fragment
        if path is not None:
            for show_iri in g.subjects(RDF.type, SCHEMA.TVSeries):
                order: int
                name: str = g.value(show_iri, path, any=False)
                rating_iri: URIRef = \
                    g.value(show_iri, SCHEMA.aggregateRating, any=False)
                rating_value: URIRef = \
                    g.value(rating_iri, SCHEMA.ratingValue, any=False)

                if query in self._clean_string(name):
                    rating: float = rating_value.toPython()
                    parts = re.split(' |-', self._clean_string(name))
                    order = int(rating * len(current_node))

                    # boost exact matches without the/a/de/...
                    for part in parts:
                        if query.startswith(part) and len(part) > 3:
                            order = sys.maxsize
                            break

                    results.append({
                        'name': name,
                        'iri': show_iri,
                        'order': order
                    })

            # All nodes visited, stop
            if current_node == query:
                return results

        # Find next node
        for o in g.objects(iri, TREE.relation):
            value: str = g.value(o, TREE.value, any=False)
            value = self._clean_string(value)
            if query.startswith(value):
                next_node: URIRef = g.value(o, TREE.node, any=False)
                next_path: URIRef = g.value(o, TREE.path, any=False)
                return self._search(query, results, next_node, next_path)

        return results

    def search(self, query) -> List[SearchResult]:
        """
        Search for TV shows on The Movie DB.
        """
        shows: List[SearchResult] = []
        parts: List[str] = re.split(' |-', query)  # spaces & '-' are separate
        results: List[Dict] = []
        debug(f'Executing search against TREE search tree for parts {parts}')

        for q in parts:
            results += self._search(self._clean_string(q), [])

        # Return list of results, sorted by popularity
        iris: List[URIRef] = []
        for r in sorted(results, key=lambda x: cast(float, x['order']),
                        reverse=True):
            i: URIRef = r['iri']
            if i not in iris:
                iris.append(i)
                shows.append(SearchResult(i, r['name'].toPython()))

        return shows

    def show(self, iri: URIRef, refresh: bool = False) -> Show:
        """
        Retrieve all information about a show from the Sunshine KG.
        """
        g: Graph = Graph()
        g_show: Graph
        g_season: Graph
        g_episode: Graph
        temp_file: _TemporaryFileWrapper = NamedTemporaryFile()
        response: Response

        debug(f'Retrieving show data for show {iri}')

        # Download and read graph from Sunshine KG
        # RDFLib can do this behind the scenes but without support for caching
        response = self._session.get(str(iri), stream=True)

        with open(temp_file.name, 'wb') as f:
            for chunk in response:
                f.write(chunk)
        g.parse(temp_file, format=RDF_FORMAT)

        g_show = Graph()
        for t in g.triples((iri, None, None)):
            g_show.add(t)

        # Store seasons
        season_iris: Iterable[Node] = g.objects(iri, SCHEMA.containsSeason)
        for season_iri in season_iris:
            season_iri = cast(URIRef, season_iri)
            g_season = Graph()

            for t in g.triples((season_iri, None, None)):
                g_season.add(t)

            self._storage.store_season(season_iri, g_season)

            # Store episodes
            episode_iris: Iterable[Node] = g.objects(season_iri,
                                                     SCHEMA.episode)
            for episode_iri in episode_iris:
                episode_iri = cast(URIRef, episode_iri)
                g_episode = Graph()
                watched_iri: URIRef

                for t in g.triples((episode_iri, None, None)):
                    g_episode.add(t)

                # Watch status, only for new shows
                watched_iri = URIRef(f'{episode_iri.toPython()}/watched')
                g_episode.add((watched_iri,
                               SCHEMA.actionStatus,
                               SCHEMA.PotentialActionStatus))
                g_episode.add((watched_iri,
                               SCHEMA.object,
                               episode_iri))
                g_episode.add((episode_iri,
                               SCHEMA.potentialAction,
                               watched_iri))

                self._storage.store_episode(episode_iri, g_episode, refresh)

        # Store locally for later re-use
        self._storage.store_show(iri, g_show)

        return Show(iri, g_show)
