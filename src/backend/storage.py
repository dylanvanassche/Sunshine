# storage.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from abc import ABC, abstractmethod
from typing import List, TYPE_CHECKING

if TYPE_CHECKING:
    from rdflib import Graph, URIRef
    from pathlib import Path


class Storage(ABC):
    """
    Abstraction of a storage of TV shows.
    All storages must implement this abstract class to be able to switch
    storages without changing anything else of the program.
    """

    def __init__(self):
        pass

    @abstractmethod
    def store_show(self, iri: URIRef, g: Graph) -> None:
        """
        Store an RDF representation of a show into the storage.
        """

    @abstractmethod
    def store_season(self, iri: URIRef, g: Graph) -> None:
        """
        Store an RDF representation of a season into the storage.
        """

    @abstractmethod
    def store_episode(self, iri: URIRef, g: Graph,
                      refresh: bool = False) -> None:
        """
        Store an RDF representation of an episode into the storage.
        Set refresh to True if you want to keep the watch status
        of the episode.
        """

    @abstractmethod
    def retrieve_shows(self) -> List[URIRef]:
        """
        Retrieve a list IRIs of shows from the storage.
        """

    @abstractmethod
    def retrieve_show(self, iri: URIRef) -> Graph:
        """
        Retrieve a show as RDF from the storage.
        """

    @abstractmethod
    def retrieve_season(self, iri: URIRef) -> Graph:
        """
        Retrieve a season as RDF from the storage.
        """

    @abstractmethod
    def retrieve_episode(self, iri: URIRef) -> Graph:
        """
        Retrieve a episode as RDF from the storage.
        """

    @abstractmethod
    def retrieve_cover(self, iri: URIRef, url: str) -> Path:
        """
        Retrieve a cover image by URL and save it into storage.
        """

    @abstractmethod
    def remove_show(self, iri: URIRef) -> None:
        """
        Remove a show by IRI from storage.
        """

    @property
    @abstractmethod
    def has_shows(self) -> bool:
        """
        Check if storage is empty or not.
        """
        pass
