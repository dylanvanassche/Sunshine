# show.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy, GdkPixbuf
from typing import TYPE_CHECKING

from ..views.show import ShowView
from ..sunshine import COVER_WIDTH, COVER_HEIGHT

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.storage import Storage
    from ..page_stack import PageStack


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/show_widget.ui')
class ShowWidget(Handy.ActionRow):
    __gtype_name__ = 'Show'

    show_container: Gtk.Widget = Gtk.Template.Child()
    title_label: Gtk.Widget = Gtk.Template.Child()
    number_of_episodes_label: Gtk.Widget = Gtk.Template.Child()
    watched_checkmark: Gtk.Widget = Gtk.Template.Child()
    cover_image: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, show: Show, page_stack: PageStack, storage: Storage,
                 **kwargs):
        super().__init__(**kwargs)
        self._show: Show = show
        self._page_stack: PageStack = page_stack
        self._storage: Storage = storage

        self.title_label.set_label(self._show.title)

        if self._show.number_of_seasons is not None:
            self.number_of_episodes_label \
                .set_label(f'{self._show.number_of_seasons} seasons')
            self.number_of_episodes_label.set_visible(True)

        if self._show.cover_path is not None:
            p: str = str(self._show.cover_path)
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(p,
                                                             COVER_WIDTH,
                                                             COVER_HEIGHT,
                                                             True)
            self.cover_image.set_from_pixbuf(pixbuf)

        self.refresh()
        self.show_all()

        self.connect('activated', self.show_description)

    def show_description(self, row):
        show: ShowView = ShowView(self._show, self._page_stack, self._storage)
        self._page_stack.push(show, 'show', is_subview=True)

    def refresh(self):
        # Dim the episode widget when watched and add checkmark
        if self._show.watched:
            self.watched_checkmark.set_visible(not self._show
                                               .has_unreleased_seasons)
            self.title_label.get_style_context().add_class('dim-label')
            self.cover_image.get_style_context().add_class('dim-label')
        # Hide checkmark and remove dim
        else:
            self.watched_checkmark.set_visible(False)
            self.title_label.get_style_context().remove_class('dim-label')
            self.cover_image.get_style_context().remove_class('dim-label')

    @property
    def iri(self):
        return self._show.iri

    @property
    def watched(self):
        return self._show.watched

    @property
    def show(self):
        return self._show
