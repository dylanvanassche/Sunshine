# new_episode.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy, GdkPixbuf
from typing import Optional, TYPE_CHECKING

from ..sunshine import COVER_WIDTH, COVER_HEIGHT

if TYPE_CHECKING:
    from rdflib import URIRef
    from datetime import date as d
    from ..backend.episode import Episode
    from ..backend.show import Show
    from ..backend.storage import Storage
    from ..views.new import NewView


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/'
                            'ui/new_episode_widget.ui')
class NewEpisodeWidget(Handy.ActionRow):
    __gtype_name__ = 'NewEpisode'

    episode_container: Gtk.Widget = Gtk.Template.Child()
    title_label: Gtk.Widget = Gtk.Template.Child()
    show_label: Gtk.Widget = Gtk.Template.Child()
    description_label: Gtk.Widget = Gtk.Template.Child()
    watched_checkmark: Gtk.Widget = Gtk.Template.Child()
    watched_button: Gtk.Widget = Gtk.Template.Child()
    cover_image: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, show: Show, episode: Episode, storage: Storage,
                 new_view: NewView, **kwargs):
        super().__init__(**kwargs)
        self._show: Show = show
        self._episode: Episode = episode
        self._storage: Storage = storage
        self._new_view: NewView = new_view

        self.title_label.set_label(self._episode.title)
        self.show_label.set_label(self._show.title)

        if self._show.cover_path is not None:
            p: str = str(self._show.cover_path)
            pixbuf = GdkPixbuf.Pixbuf.new_from_file_at_scale(p,
                                                             COVER_WIDTH,
                                                             COVER_HEIGHT,
                                                             True)
            self.cover_image.set_from_pixbuf(pixbuf)

        self.refresh()

        self.connect('activated', self.show_description)
        self.description_label.set_visible(False)

        self.watched_button.connect('clicked', self.edit_watch_state)

        self.show_all()

    def refresh(self) -> None:
        # Dim the episode widget when watched and add checkmark
        if self._episode.watched:
            self.watched_checkmark.set_visible(True)
            self.watched_button.set_visible(False)
            self.title_label.get_style_context().add_class('dim-label')
            self.cover_image.get_style_context().add_class('dim-label')
        # Hide checkmark and remove dim
        else:
            self.watched_checkmark.set_visible(False)
            self.watched_button.set_visible(True)
            self.title_label.get_style_context().remove_class('dim-label')
            self.cover_image.get_style_context().remove_class('dim-label')

    @property
    def watched(self) -> bool:
        return self._episode.watched

    def show_description(self, row) -> None:
        if self._episode.description is not None and self._episode.description:
            self.description_label.set_label(self._episode.description)
            self.description_label.set_visible(not self.description_label
                                               .get_visible())

    def edit_watch_state(self, button) -> None:
        self._episode.watched = True
        self.refresh()
        self._new_view.refresh()

    @property
    def iri(self) -> URIRef:
        return self._episode.iri

    @property
    def date(self) -> Optional[d]:
        return self._episode.date
