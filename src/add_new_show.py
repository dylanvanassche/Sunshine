# add_new_show.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, Handy, Gdk, GLib
from typing import List, Optional, TYPE_CHECKING
from logging import info
from threading import Thread

from .backend.kg import KG

if TYPE_CHECKING:
    from rdflib import URIRef
    from datetime import date as d
    from .backend.datasource import Datasource
    from .backend.search_result import SearchResult as SR

SEARCH_DELAY_MS = 400


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/search_widget.ui')
class SearchResult(Handy.ActionRow):
    __gtype_name__ = 'SearchResult'

    title_label: Gtk.Label = Gtk.Template.Child()
    year_label: Gtk.Label = Gtk.Template.Child()
    selection_icon: Gtk.Image = Gtk.Template.Child()

    def __init__(self, iri: URIRef, title: str, date: d,
                 already_added: bool, query: str):
        super().__init__()
        self._iri: URIRef = iri
        self._already_added: bool = already_added
        self._query: str = query
        self._title: str = title

        self.title_label.set_label(self._title)

        if date is None:
            self.year_label.set_label('')
        else:
            self.year_label.set_label(str(date.year))

        # Avoid to add show if already in library
        if self._already_added:
            self.selection_icon.set_visible(True)
            self.title_label.get_style_context().add_class('dim-label')
            self.year_label.get_style_context().add_class('dim-label')
        else:
            self.selection_icon.set_visible(False)
            self.title_label.get_style_context().remove_class('dim-label')
            self.year_label.get_style_context().remove_class('dim-label')

        self.set_visible(True)

    @property
    def iri(self):
        return self._iri

    def select(self):
        """
        Mark row as selected
        """
        self.selection_icon.set_visible(True)

    def unselect(self):
        """
        Mark row as unselected
        """
        self.selection_icon.set_visible(False)

    def is_selected(self):
        """
        Check if row is already selected
        """
        return self.selection_icon.is_visible()


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine'
                            '/ui/add_show_dialog.ui')
class AddNewShowDialog(Gtk.Dialog):
    __gtype_name__ = 'AddNewShowDialog'

    search: Gtk.SearchEntry = Gtk.Template.Child()
    listbox: Gtk.ListBox = Gtk.Template.Child()
    empty_search_box: Gtk.Box = Gtk.Template.Child()
    add_button: Gtk.Box = Gtk.Template.Child()
    stack: Gtk.Stack = Gtk.Template.Child()

    def __init__(self, existing_shows: List[URIRef], **kwargs):
        super().__init__(use_header_bar=True, **kwargs)
        self.search.connect('search-changed', self.on_search)
        self._database: Datasource = KG()
        self._shows: Optional[List[SR]] = None
        self._existing_shows: List[URIRef] = existing_shows
        self._selected_show: Optional[SR] = None
        self._selected_row: Handy.ActionRow = None
        self._search_text: str = ''
        self._last_search_text: str = ''
        self._search_timer: int = 0
        self.connect('key_press_event', self._on_key_press)

    def select_show(self, row):
        # Ignore selecting existing shows
        if row.iri in self._existing_shows:
            return

        # Allow to unselect a selected row
        if row.is_selected():
            row.unselect()
            self.add_button.set_sensitive(False)
            self._selected_show = None
            self._selected_row = None
            return

        # Unselect all other rows and select the new row
        if self._selected_row is not None:
            self._selected_row.unselect()
        row.select()
        self.add_button.set_sensitive(True)

        # Match row with show
        for show in self._shows:
            if row.iri == show.iri:
                self._selected_show = show
                self._selected_row = row
                break

    def _on_key_press(self, _, event):
        _, key_val = event.get_keyval()

        # Close window when ESC is pressed
        if key_val == Gdk.KEY_Escape:
            self.destroy()

    @property
    def selected_show(self):
        return self._selected_show

    def _update_results_widget(self, query):
        # Clear list
        for child in self.listbox.get_children():
            self.listbox.remove(child)

        # Fill list again
        if self._shows:
            query = GLib.markup_escape_text(query, len(query))

            for show in self._shows:
                already_added: bool = show.iri in self._existing_shows
                s = SearchResult(show.iri, show.title, show.date,
                                 already_added, query)
                s.connect('activated', self.select_show)
                self.listbox.add(s)

            # Results updated, show them again
            self.listbox.set_visible(True)
            self.stack.set_visible_child_name('results')
        else:
            # Results updated, no results
            self.listbox.set_visible(False)
            self.stack.set_visible_child_name('no-results')

        return False

    def search_shows(self, query: str):
        self._shows = self._database.search(query)
        GLib.idle_add(self._update_results_widget, query)

    def execute_search(self):
        # Clear timer
        if self._search_timer:
            GLib.Source.remove(self._search_timer)
            self._search_timer = 0

        # Do not search multiple times for the same
        if self._search_text == self._last_search_text:
            return False
        self._last_search_text = self._search_text

        # Hide results box until we performed the query
        self.listbox.set_visible(False)

        # Show placeholder when no search query is provided
        if not self._search_text:
            self.stack.set_visible_child_name('empty')
            info('Search query is empty')
            return False

        # Indicate that search is started
        self.stack.set_visible_child_name('loading')

        # Perform search on datasource
        info(f'Executing search for: {self._search_text}')
        thread: Thread = Thread(target=self.search_shows,
                                args=(self._search_text,))
        thread.daemon = True
        thread.start()
        return False

    def on_search(self, search):
        """
        Search text callback
        """
        # Kill the existing debouncer timer as the text has been updated
        if self._search_timer:
            GLib.Source.remove(self._search_timer)
            self._search_timer = 0

        self._search_text = search.get_text()

        # Restart the debouncer timer
        self._search_timer = GLib.timeout_add(SEARCH_DELAY_MS,
                                              self.execute_search)
