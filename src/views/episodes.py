# episodes.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk
from typing import List, TYPE_CHECKING

from ..widgets.episode import EpisodeWidget

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.season import Season
    from ..backend.episode import Episode
    from ..backend.storage import Storage


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/episodes.ui')
class EpisodesView(Gtk.ScrolledWindow):
    __gtype_name__ = 'EpisodesView'

    episodes_list: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, season: Season, storage: Storage, show: Show, **kwargs):
        super().__init__(**kwargs)
        self._season: Season = season
        self._storage: Storage = storage
        self._show: Show = show

        if self._season.episodes is not None:
            episodes: List[Episode] = self._season.episodes
            for episode in episodes:
                widget: EpisodeWidget = EpisodeWidget(episode, self._storage)
                self.episodes_list.add(widget)
        self.episodes_list.show_all()

        self.show_all()

    @property
    def watched(self):
        return self._season.watched

    @watched.setter
    def watched(self, state: bool):
        self._season.watched = state
        self.refresh()

    def refresh(self):
        for episode in self.episodes_list.get_children():
            episode.refresh()

    @property
    def show(self):
        return self._show
