# library.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk, GLib
from typing import List, TYPE_CHECKING
from logging import debug

from ..widgets.show import ShowWidget

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.storage import Storage
    from ..page_stack import PageStack


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/library.ui')
class LibraryView(Gtk.ScrolledWindow):
    __gtype_name__ = 'LibraryView'

    watched_list: Gtk.Widget = Gtk.Template.Child()
    unwatched_list: Gtk.Widget = Gtk.Template.Child()
    watched_box: Gtk.Widget = Gtk.Template.Child()
    unwatched_box: Gtk.Widget = Gtk.Template.Child()
    placeholder: Gtk.Widget = Gtk.Template.Child()
    placeholder_title_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_description_label: Gtk.Widget = Gtk.Template.Child()
    placeholder_image: Gtk.Widget = Gtk.Template.Child()

    def __init__(self, page_stack: PageStack, storage: Storage, **kwargs):
        super().__init__(**kwargs)
        self._page_stack: PageStack = page_stack
        self._storage: Storage = storage
        self.set_name('library')
        self.show_all()

    def add_show(self, show: Show):
        widget: ShowWidget = ShowWidget(show, self._page_stack, self._storage)
        if show.watched:
            self.watched_list.add(widget)
        else:
            self.unwatched_list.add(widget)
        self.placeholder.set_visible(False)

        if self.watched_list.get_children():
            self.watched_box.set_visible(True)
        else:
            self.watched_box.set_visible(False)

        if self.unwatched_list.get_children():
            self.unwatched_box.set_visible(True)
        else:
            self.unwatched_box.set_visible(False)

    def replace_show(self, show: Show):
        self.remove_show(show)
        self.add_show(show)

    def remove_show(self, show: Show):
        has_children = False
        found = False
        for listbox in [self.watched_list, self.unwatched_list]:
            for widget in listbox.get_children():
                if widget.iri == show.iri:
                    listbox.remove(widget)
                    has_children = True
                    found = True
                    break

                if found:
                    break

        # Not empty
        if has_children:
            self.placeholder.set_visible(False)
            self.placeholder_title_label.set_visible(False)
            self.placeholder_description_label.set_visible(False)
            self.placeholder_image.get_style_context() \
                .remove_class('dim-label')
        # Empty again
        else:
            self.placeholder.set_visible(True)
            self.placeholder_title_label.set_visible(True)
            self.placeholder_description_label.set_visible(True)
            self.placeholder_image.get_style_context() \
                .add_class('dim-label')

    def loading(self, state: bool):
        if state:
            self.placeholder_title_label.set_visible(False)
            self.placeholder_description_label.set_visible(False)
            self.placeholder_image.get_style_context() \
                .remove_class('dim-label')
        else:
            self.placeholder_title_label.set_visible(True)
            self.placeholder_description_label.set_visible(True)
            self.placeholder_image.get_style_context() \
                .add_class('dim-label')

    def refresh(self):
        debug('Refreshing library view')
        shows_to_move = []

        for show in self.watched_list.get_children():
            show.refresh()
            if not show.watched:
                shows_to_move.append(show)

        for show in self.unwatched_list.get_children():
            show.refresh()
            if show.watched:
                shows_to_move.append(show)

        # Doing the move doing refresh causes issues with GTK
        # Updating widgets while also destroying them is not a good idea
        GLib.idle_add(self._move, shows_to_move)

    def _move(self, shows_to_move: List[ShowWidget]):
        for show in shows_to_move:
            self.replace_show(show.show)
