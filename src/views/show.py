# show.py
#
# Copyright 2021 Dylan Van Assche
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from __future__ import annotations

from gi.repository import Gtk
from typing import TYPE_CHECKING

from ..widgets.season import SeasonWidget

if TYPE_CHECKING:
    from ..backend.show import Show
    from ..backend.storage import Storage
    from ..page_stack import PageStack


@Gtk.Template(resource_path='/be/dylanvanassche/sunshine/ui/show.ui')
class ShowView(Gtk.ScrolledWindow):
    __gtype_name__ = 'ShowView'

    description: Gtk.Widget = Gtk.Template.Child()
    seasons: Gtk.Widget = Gtk.Template.Child()
    cover: Gtk.Image = Gtk.Template.Child()

    def __init__(self, show: Show, page_stack: PageStack, storage: Storage,
                 **kwargs):
        super().__init__(**kwargs)
        self._show: Show = show
        self._page_stack: PageStack = page_stack
        self._storage: Storage = storage

        # Cover, description and seasons are optional for a show
        if self._show.cover_path is not None:
            self.cover.set_from_file(str(self._show.cover_path))

        if self._show.description is not None:
            self.description.set_label(self._show.description)

        if self._show.seasons is not None:
            for season in self._show.seasons:
                s: SeasonWidget = SeasonWidget(season, self._page_stack,
                                               self._storage, self._show)
                self.seasons.add(s)
            self.seasons.show_all()

        self.show_all()

    @property
    def watched(self):
        return self._show.watched

    @watched.setter
    def watched(self, state: bool):
        self._show.watched = state
        self.refresh()

    def refresh(self):
        for season in self.seasons.get_children():
            season.refresh()

    @property
    def show(self):
        return self._show
