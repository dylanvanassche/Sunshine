import unittest
import unittest.mock as mock
from xdg import BaseDirectory
from os import makedirs
from shutil import copyfile
from rdflib import Graph, URIRef
from datetime import date

from src.backend.season import Season
from .mocking import save_data_path_mocked

IRI = 'https://sunshine.dylanvanassche.be/show/60735/season/90247'
DESCRIPTION = 'The mission of Barry Allen, aka The Flash, is once more to'


class SeasonTest(unittest.TestCase):

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_graph(self):
        graph = Graph().parse('./tests/data/season.nt')
        season = Season(URIRef(IRI), graph)
        self.assertEqual(season.iri, URIRef(IRI))
        self.assertEqual(season.title, 'Season 4')
        self.assertEqual(season.number, 4)
        self.assertEqual(season.date, date(2017, 10, 10))
        self.assertEqual(type(season.description), str)
        self.assertTrue(season.description.startswith(DESCRIPTION))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_storage(self):
        path = BaseDirectory.save_data_path('sunshine/season/')
        makedirs(path, exist_ok=True)
        copyfile('./tests/data/season.nt', path + '90247')
        season = Season(URIRef(IRI))
        self.assertEqual(season.iri, URIRef(IRI))
        self.assertEqual(season.title, 'Season 4')
        self.assertEqual(season.number, 4)
        self.assertEqual(season.date, date(2017, 10, 10))
        self.assertEqual(type(season.description), str)
        self.assertTrue(season.description.startswith(DESCRIPTION))
