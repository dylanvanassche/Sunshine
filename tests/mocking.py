from tempfile import gettempdir
from logging import info


def save_data_path_mocked(path):
    path = f'{gettempdir()}/{path}'
    info(f'Mocking data directories at "{path}"')
    return path
