import unittest
import unittest.mock as mock
from rdflib import Graph, URIRef
from rdflib.compare import to_isomorphic

from src.backend.local import LocalStorage
from .mocking import save_data_path_mocked

SHOW_IRI = 'https://sunshine.dylanvanassche.be/show/62688'
SEASON_IRI = 'https://sunshine.dylanvanassche.be/show/60735/season/90247'
EPISODE_IRI = 'https://sunshine.dylanvanassche.be/show/1412/season/3697' \
              '/episode/64129'


class LocalStorageTest(unittest.TestCase):

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_store_retrieve_show(self):
        graph = Graph().parse('./tests/data/show.nt')
        storage = LocalStorage.get_instance()
        storage.store_show(URIRef(SHOW_IRI), graph)
        show_graph = storage.retrieve_show(URIRef(SHOW_IRI))
        self.assertEqual(to_isomorphic(graph), to_isomorphic(show_graph))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_store_retrieve_season(self):
        graph = Graph().parse('./tests/data/season.nt')
        storage = LocalStorage.get_instance()
        storage.store_season(URIRef(SEASON_IRI), graph)
        season_graph = storage.retrieve_season(URIRef(SEASON_IRI))
        self.assertEqual(to_isomorphic(graph), to_isomorphic(season_graph))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_store_retrieve_episode(self):
        graph = Graph().parse('./tests/data/episode.nt')
        storage = LocalStorage.get_instance()
        storage.store_episode(URIRef(EPISODE_IRI), graph)
        episode_graph = storage.retrieve_episode(URIRef(EPISODE_IRI))
        self.assertEqual(to_isomorphic(graph), to_isomorphic(episode_graph))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_retrieve_shows(self):
        graph = Graph().parse('./tests/data/show.nt')
        storage = LocalStorage.get_instance()
        storage.store_show(URIRef(SHOW_IRI), graph)
        shows_list = storage.retrieve_show(URIRef(SHOW_IRI))
        self.assertTrue(shows_list)

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_retrieve_cover(self):
        graph = Graph().parse('./tests/data/show.nt')
        storage = LocalStorage.get_instance()
        storage.store_show(URIRef(SHOW_IRI), graph)
        show_graph = storage.retrieve_show(URIRef(SHOW_IRI))
        self.assertEqual(to_isomorphic(graph), to_isomorphic(show_graph))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_remove_show(self):
        graph = Graph().parse('./tests/data/show.nt')
        storage = LocalStorage.get_instance()
        storage.store_show(URIRef(SHOW_IRI), graph)
        self.assertTrue(storage.has_shows)
        storage.remove_show(URIRef(SHOW_IRI))
        self.assertFalse(storage.has_shows)

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_has_shows(self):
        graph = Graph().parse('./tests/data/show.nt')
        storage = LocalStorage.get_instance()
        storage.store_show(URIRef(SHOW_IRI), graph)
        self.assertTrue(storage.has_shows)
