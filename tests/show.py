import unittest
import unittest.mock as mock
from xdg import BaseDirectory
from os import makedirs
from shutil import copyfile
from rdflib import Graph, URIRef
from datetime import date

from src.backend.show import Show
from .mocking import save_data_path_mocked

IRI = 'https://sunshine.dylanvanassche.be/show/62688'
DESCRIPTION = 'Twenty-four-year-old Kara Zor-El, who was taken in by the'


class ShowTest(unittest.TestCase):

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_graph(self):
        graph = Graph().parse('./tests/data/show.nt')
        show = Show(URIRef(IRI), graph)
        self.assertEqual(show.iri, URIRef(IRI))
        self.assertEqual(show.title, 'Supergirl')
        self.assertEqual(show.date, date(2015, 10, 26))
        self.assertEqual(type(show.description), str)
        self.assertTrue(show.description.startswith(DESCRIPTION))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_storage(self):
        path = BaseDirectory.save_data_path('sunshine/show/')
        makedirs(path, exist_ok=True)
        copyfile('./tests/data/show.nt', path + '62688')
        show = Show(URIRef(IRI))
        self.assertEqual(show.iri, URIRef(IRI))
        self.assertEqual(show.title, 'Supergirl')
        self.assertEqual(show.date, date(2015, 10, 26))
        self.assertEqual(type(show.description), str)
        self.assertTrue(show.description.startswith(DESCRIPTION))
