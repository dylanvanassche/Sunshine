import unittest
from rdflib import URIRef

from src.backend.kg import KG

IRI = 'https://sunshine.dylanvanassche.be/show/62688'


class KGTest(unittest.TestCase):

    def test_search(self):
        kg = KG()
        results = kg.search('superg')

        try:
            show = next(filter(lambda x: x.title == 'Supergirl', results))
            self.assertEqual(show.title, 'Supergirl')
            self.assertEqual(show.iri, URIRef(IRI))
        except StopIteration:
            self.fail('Show "Supergirl" not found in search results')

    def test_show(self):
        kg = KG()
        show = kg.show(URIRef(IRI))

        self.assertEqual(show.title, 'Supergirl')
        self.assertEqual(show.iri, URIRef(IRI))

    def test_show_refresh(self):
        kg = KG()
        show = kg.show(URIRef(IRI), refresh=True)

        self.assertEqual(show.title, 'Supergirl')
        self.assertEqual(show.iri, URIRef(IRI))
