import unittest
from rdflib import URIRef
from datetime import date as d

from src.backend.search_result import SearchResult

IRI = 'https://sunshine.dylanvanassche.be/show/62688'


class SearchResultTest(unittest.TestCase):

    def test_create_without_date(self):
        search_result = SearchResult(URIRef(IRI), 'Supergirl')

        self.assertEqual(search_result.iri, URIRef(IRI))
        self.assertEqual(search_result.title, 'Supergirl')
        self.assertEqual(search_result.date, None)

    def test_create_with_date(self):
        today = d.today()
        search_result = SearchResult(URIRef(IRI), 'Supergirl', today)

        self.assertEqual(search_result.iri, URIRef(IRI))
        self.assertEqual(search_result.title, 'Supergirl')
        self.assertEqual(search_result.date, today)
