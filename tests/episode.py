import unittest
import unittest.mock as mock
from xdg import BaseDirectory
from os import makedirs
from shutil import copyfile
from rdflib import Graph, URIRef
from datetime import date

from src.backend.episode import Episode
from .mocking import save_data_path_mocked

IRI = 'https://sunshine.dylanvanassche.be/show/1412/season/3697/episode/64129'
DESCRIPTION = 'When a young woman meets a violent death after partying at'


class EpisodeTest(unittest.TestCase):

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_graph(self):
        graph = Graph().parse('./tests/data/episode.nt')
        episode = Episode(URIRef(IRI), graph)
        self.assertEqual(episode.iri, URIRef(IRI))
        self.assertEqual(episode.title, 'Unfinished Business')
        self.assertEqual(episode.number, 19)
        self.assertEqual(episode.date, date(2013, 4, 3))
        self.assertEqual(type(episode.description), str)
        self.assertTrue(episode.description.startswith(DESCRIPTION))

    @mock.patch('xdg.BaseDirectory.save_data_path', save_data_path_mocked)
    def test_create_from_storage(self):
        path = BaseDirectory.save_data_path('sunshine/episode/')
        makedirs(path, exist_ok=True)
        copyfile('./tests/data/episode.nt', path + '64129')
        episode = Episode(URIRef(IRI))
        self.assertEqual(episode.iri, URIRef(IRI))
        self.assertEqual(episode.title, 'Unfinished Business')
        self.assertEqual(episode.number, 19)
        self.assertEqual(episode.date, date(2013, 4, 3))
        self.assertEqual(type(episode.description), str)
        self.assertTrue(episode.description.startswith(DESCRIPTION))
